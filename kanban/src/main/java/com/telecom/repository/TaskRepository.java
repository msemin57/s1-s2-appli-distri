package com.telecom.repository;
import com.telecom.domainmodel.Developer;
import com.telecom.domainmodel.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}