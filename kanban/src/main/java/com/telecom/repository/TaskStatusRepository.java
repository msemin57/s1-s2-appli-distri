package com.telecom.repository;
import com.telecom.domainmodel.Developer;
import com.telecom.domainmodel.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface TaskStatusRepository extends JpaRepository<TaskStatus, Long> {
}