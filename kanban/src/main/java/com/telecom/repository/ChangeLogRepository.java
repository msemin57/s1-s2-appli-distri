package com.telecom.repository;
import com.telecom.domainmodel.ChangeLog;
import org.springframework.data.repository.CrudRepository;
import com.telecom.domainmodel.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChangeLogRepository extends JpaRepository<ChangeLog, Long> {
}