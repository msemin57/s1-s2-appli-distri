package com.telecom.domainmodel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import java.time.LocalDate;


@Inheritance
@Entity
@Getter
@Setter
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	private String title;
	private Integer nbHoursForecast;
	private Integer nbHoursReal;
	private LocalDate created;

	@ManyToOne
	private TaskType type;
	@ManyToOne
	private TaskStatus status;

	@ManyToMany
	private List<Developer> Developers;

	public Task(long id, String title, Integer nbHoursForecast, Integer nbHoursReal, LocalDate created, TaskType type, TaskStatus status) {
		this.id = id;
		this.title = title;
		this.nbHoursForecast = nbHoursForecast;
		this.nbHoursReal = nbHoursReal;
		this.created = created;
		this.type = type;
		this.status = status;
		this.Developers = new ArrayList<>();
	}

	public void addDeveloper(Developer developer) {
		this.Developers.add(developer);
		developer.addTask(this);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getNbHoursForecast() {
		return nbHoursForecast;
	}

	public void setNbHoursForecast(Integer nbHoursForecast) {
		this.nbHoursForecast = nbHoursForecast;
	}

	public Integer getNbHoursReal() {
		return nbHoursReal;
	}

	public void setNbHoursReal(Integer nbHoursReal) {
		this.nbHoursReal = nbHoursReal;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public TaskType getType() {
		return type;
	}

	public void setType(TaskType type) {
		this.type = type;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public List<Developer> getDevelopers() {
		return Developers;
	}

	public void setDevelopers(List<Developer> developers) {
		this.Developers = developers;
	}
}
