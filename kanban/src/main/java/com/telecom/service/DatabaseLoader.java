package com.telecom.service;

import com.telecom.domainmodel.*;
import com.telecom.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DatabaseLoader implements CommandLineRunner{

    private final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);

    @Autowired
    ChangeLogRepository changeLogRepository;

    @Autowired
    DeveloperRepository developerRepository;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    TaskStatusRepository taskStatusRepository;

    @Autowired
    TaskTypeRepository taskTypeRepository;

    @Override
    public void run (String... args) throws Exception
    {
        logger.info("Loading data from DatabaseLoader.....");

        LocalDate now = LocalDate.now();

        Developer devMS = new Developer("Mathieu", "Semin", "motdepasse", "mail@mail.com", LocalDate.of(1999, 1, 24));
        Developer devEVEY = new Developer("Clement", "Devevey", "motdepasse2", "mail@mail.com", LocalDate.of(2001, 10, 10));
        Developer devA = new Developer("Eva", "Luvisutto", "motdepasse3", "mail@mail.com", LocalDate.of(1999, 9, 23));

        TaskStatus toDo = new TaskStatus(0,"ToDo");
        TaskStatus doing = new TaskStatus(1,"Doing");
        TaskStatus finished = new TaskStatus(2,"Finished");

        TaskType type1 = new TaskType(0,"Type 1");
        TaskType type2 = new TaskType(1,"Type 2");

        Task task1 = new Task(0,"Task 1", 2, 5, now, type1, finished);
        Task task2 = new Task(1,"Task 2", 30, 5, now, type2, doing);

        task1.addDeveloper(devMS);
        task1.addDeveloper(devEVEY);
        task2.addDeveloper(devA);

        ChangeLog changeLog1 = new ChangeLog(0, now, task1, doing, finished);
        ChangeLog changeLog2 = new ChangeLog(1, now, task2, toDo, doing);

        // Sauvegarde dans la base de données
        changeLogRepository.save(changeLog1);
        changeLogRepository.save(changeLog2);

        developerRepository.save(devMS);
        developerRepository.save(devEVEY);
        developerRepository.save(devA);

        taskStatusRepository.save(toDo);
        taskStatusRepository.save(doing);
        taskStatusRepository.save(finished);

        taskTypeRepository.save(type1);
        taskTypeRepository.save(type2);

        taskRepository.save(task1);
        taskRepository.save(task2);
    }
}
