package com.telecom.service.implementation;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telecom.domainmodel.Task;
import com.telecom.domainmodel.TaskStatus;
import com.telecom.repository.TaskRepository;
import com.telecom.service.TaskService;

public class TaskServiceImplementation implements TaskService
{

    @Autowired
    private TaskRepository taskRepo;

    @Override
    public Collection<Task> findAllTasks()
    {
        return taskRepo.findAll();
    }
    @Override
    public Task findTask(long id)
    {
        Optional<Task> result = taskRepo.findById(id);

        if (result.isPresent())
        {
            return result.get();
        }
        else
        {
            return null;
        }
    }
    @Override
    public Task moveRightTask(Task task)
    {
        switch (task.getStatus().getLabel())
        {
            case "ToDo" : task.setStatus(new TaskStatus(1,"Doing")); break;
            case "Doing" : task.setStatus(new TaskStatus(2,"Finished")); break;
            case "Finished" : task.setStatus(new TaskStatus(0,"ToDo")); break;
            default: break;
        }
        return task;
    }
    @Override
    public Task moveLeftTask(Task task)
    {
        switch (task.getStatus().getLabel())
        {
            case "ToDo" : task.setStatus(new TaskStatus(2,"Finished")); break;
            case "Doing" : task.setStatus(new TaskStatus(0,"ToDo")); break;
            case "Finished" : task.setStatus(new TaskStatus(1,"Doing")); break;
            default: break;
        }
        return task;
    }
}
