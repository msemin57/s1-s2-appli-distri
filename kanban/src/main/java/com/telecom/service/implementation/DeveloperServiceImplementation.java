package com.telecom.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telecom.domainmodel.Developer;
import com.telecom.repository.DeveloperRepository;
import com.telecom.service.DeveloperService;

@Service
public class DeveloperServiceImplementation implements DeveloperService
{
    @Autowired
    private DeveloperRepository developerRepositoryRepo;
    @Override
    public List<Developer> findAllDeveloper()
    {
        return this.developerRepositoryRepo.findAll();
    }
}
