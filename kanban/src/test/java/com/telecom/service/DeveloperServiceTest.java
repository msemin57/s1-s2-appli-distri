package com.telecom.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.telecom.domainmodel.Developer;
import com.telecom.service.implementation.DeveloperServiceImplementation;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
@SpringBootTest
class DeveloperServiceTest {

    @Autowired
    private DeveloperServiceImplementation devService;

    @Test
    void findAllDeveloperTest() {
        Developer devMS = new Developer("Mathieu", "Semin", "motdepasse", "mail@mail.com", LocalDate.of(1999, 1, 24));
        Developer devEVEY = new Developer("Clement", "Devevey", "motdepasse2", "mail@mail.com", LocalDate.of(2001, 10, 10));
        Developer devA = new Developer("Eva", "Luvisutto", "motdepasse3", "mail@mail.com", LocalDate.of(1999, 9, 23));

        List<Developer> expected = new ArrayList<>();
        expected.add(devMS);
        expected.add(devEVEY);
        expected.add(devA);

        List<Developer> result = devService.findAllDeveloper();

        var testBool = result.containsAll(expected);

        assertEquals(true, testBool);
    }
}
