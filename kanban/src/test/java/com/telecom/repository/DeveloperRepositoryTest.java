package com.telecom.repository;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.telecom.domainmodel.Developer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ContextConfiguration
@SpringBootTest
class DeveloperRepositoryTest {

    @Autowired
    DeveloperRepository developerRepository;

    @Test
    void findAllTest() {
        Developer devMS = new Developer("Mathieu", "Semin", "motdepasse", "mail@mail.com", LocalDate.of(1999, 1, 24));
        Developer devEVEY = new Developer("Clement", "Devevey", "motdepasse2", "mail@mail.com", LocalDate.of(2001, 10, 10));
        Developer devA = new Developer("Eva", "Luvisutto", "motdepasse3", "mail@mail.com", LocalDate.of(1999, 9, 23));

        List<Developer> expected = new ArrayList<Developer>();
        expected.add(devMS);
        expected.add(devEVEY);
        expected.add(devA);

        List<Developer> result = developerRepository.findAll();

        var testBool = result.containsAll(expected);

        assertEquals(true, testBool);
    }
}
