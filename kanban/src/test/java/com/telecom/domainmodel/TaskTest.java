package com.telecom.domainmodel;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import com.telecom.domainmodel.Developer;
import com.telecom.domainmodel.Task;
import com.telecom.domainmodel.TaskStatus;
import com.telecom.domainmodel.TaskType;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TaskTest {

    @Test
    public void addDeveloperTest() {
        LocalDate now = LocalDate.now();

        TaskType type1 = new TaskType(0, "type1");
        TaskStatus finished = new TaskStatus(0,"finished");

        Task taskTest = new Task(0,"Task 1", 2, 5, now, type1, finished);

        Developer devMS = new Developer("Mathieu", "Semin", "motdepasse", "mail@mail.com", LocalDate.of(1999, 1, 24));
        Developer devEVEY = new Developer("Clement", "Devevey", "motdepasse2", "mail@mail.com", LocalDate.of(2001, 10, 10));
        Developer devA = new Developer("Eva", "Luvisutto", "motdepasse3", "mail@mail.com", LocalDate.of(1999, 9, 23));

        taskTest.addDeveloper(devMS);
        taskTest.addDeveloper(devEVEY);
        taskTest.addDeveloper(devA);

        Set<Developer> expected = new HashSet<>();

        expected.add(devMS);
        expected.add(devEVEY);
        expected.add(devA);

        List<Developer> result = taskTest.getDevelopers();

        var testBool = result.containsAll(expected);

        assertEquals(true, testBool);
    }

}
